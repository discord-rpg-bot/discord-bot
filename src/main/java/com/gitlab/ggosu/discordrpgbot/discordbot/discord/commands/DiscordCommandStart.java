package com.gitlab.ggosu.discordrpgbot.discordbot.discord.commands;

import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent;
import discord4j.core.event.domain.interaction.SelectMenuInteractionEvent;
import discord4j.core.object.command.Interaction;
import discord4j.core.object.component.ActionRow;
import discord4j.core.object.component.SelectMenu;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.concurrent.TimeoutException;

@Component
public class DiscordCommandStart implements DiscordCommand {

    @Override
    public String getName() {
        return "start";
    }

    @Override
    public Mono<Void> handle(ChatInputInteractionEvent event) {
        GatewayDiscordClient client = event.getClient();

        Interaction interaction = event.getInteraction();
        //generate unique id for the interaction
        String selectionId = "characterClass-" + interaction.getId().asString();

        SelectMenu characterClassSelection = SelectMenu.of(selectionId,
                SelectMenu.Option.of("Warrior", "warrior"),
                SelectMenu.Option.of("Mage", "mage"),
                SelectMenu.Option.of("Archer", "archer"),
                SelectMenu.Option.of("Assassin", "assassin")
        ).withPlaceholder("Please select your character class");

        Mono <Void> tempListener = client.on(SelectMenuInteractionEvent.class, listEvent ->{
            if(listEvent.getCustomId().equals(selectionId)){
                String values = listEvent.getValues().toString().replace("[", "").replace("]", "");
                event.deleteReply().block();
                // todo: send message to user service to create character with template class (if 200 then reply with message)
                return listEvent.reply()
                        .withEphemeral(false)
                        .withContent("You start your journey as " + values);
            }else {
                return Mono.empty();
            }
        }).timeout(Duration.ofMinutes(30))
          .onErrorResume(TimeoutException.class, ignore -> Mono.empty())
          .then();

        return event.reply()
                .withEphemeral(false)
                .withComponents(ActionRow.of(characterClassSelection))
                .then(tempListener);
    }
}
