package com.gitlab.ggosu.discordrpgbot.discordbot;

import discord4j.core.DiscordClient;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.object.presence.ClientActivity;
import discord4j.core.object.presence.ClientPresence;
import discord4j.rest.RestClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication

public class Main {

	public static void main(String[] args) {
		SpringApplication.run(Main.class, args);
	}

	//Discord Bot Integration
	private final String token = "REPLACE IT WITH YOUR DISCORD PRIVATE KEY";

	@Bean
	public GatewayDiscordClient gatewayDiscordClient() {
		GatewayDiscordClient client = DiscordClient.create(token).gateway()
				.setInitialPresence(ignore -> ClientPresence.online(ClientActivity.competing("Evolution")))
				.login()
				.block();
		return client;
	}

	@Bean
	public RestClient discordRestClient(GatewayDiscordClient client) {
		return client.getRestClient();
	}

}
