package com.gitlab.ggosu.discordrpgbot.discordbot.discord.commands;

import discord4j.core.event.domain.interaction.ChatInputInteractionEvent;
import discord4j.core.object.command.Interaction;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class DiscordCommandDebug implements DiscordCommand {

    @Override
    public String getName() {
        return "debug";
    }

    @Override
    public Mono<Void> handle(ChatInputInteractionEvent event) {
        Interaction interaction = event.getInteraction();

        String userId =  interaction.getUser().getId().asString();
        String channelId = interaction.getChannelId().asString();
        String guildId = interaction.getGuildId().get().asString();

        return event.reply()
                .withEphemeral(false)//private message if true
                .withContent("userId: " + userId + " channelId: " + channelId + " guildId: " + guildId);

    }
}
