package com.gitlab.ggosu.discordrpgbot.discordbot.discord.commands;

import discord4j.core.event.domain.interaction.ChatInputInteractionEvent;
import reactor.core.publisher.Mono;

public interface DiscordCommand {
    String getName();
    Mono<Void> handle(ChatInputInteractionEvent event);
}
