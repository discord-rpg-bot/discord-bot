# discord-bot
handles user requests from discord app

Requires name server to be running on port 9000

Runs on port 8000

You have to put your own private Discord token in Main Class

You can create a bot and generate token here: [https://discord.com/developers/applications](https://discord.com/developers/applications)

Using Discord Wrapper Discord4J

Documentation: [https://docs.discord4j.com/](https://docs.discord4j.com/)